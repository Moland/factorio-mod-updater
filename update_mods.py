import hashlib
import json
import os
import re
import sys

import requests


FACTORIO_DIR = "/opt/factorio/"
PLAYER_DATA = "player-data.json"
MOD_DIR = "mods/"
MOD_LIST = "mod-list.json"

BASE_URL = "https://mods.factorio.com"
API_ENDPOINT = "/api/mods"
EXTENDED_INFO = "/full"

SETTINGS_FILE = "update-info.json"


DEV_MSG = "### If this message is appearing, then it means something is being developed, and should be disregarded. ###"


# Check for the presence of a directory
def check_dir_exists(dirpath):
    return os.path.isdir(dirpath)

# Check for the presence of a file
def check_file_exists(filepath):
    return os.path.isfile(filepath)

# Lists everything in a specified directory.
def list_dir(dirpath, *args):
    files = None

    try:
        files = os.listdir(dirpath)
    except:
        pass

    if 'json' in args:
        pass

    return files

# Opens and returns if a file is found on the specified path.
def open_file(filepath, *args):
    data = None

    if 'json' in args:
        with open(filepath) as json_file:
            data = json.load(json_file)

    return data

# Writes data to filepath, write-mode can be supplied as argument.
def write_file(filepath, data, *args):
    if args:
        mode = args[0]
    else:
        mode = 'w'

    with open(filepath, mode) as mod_file:
        mod_file.write(data)

# Removes file from disk.
def remove_file(filepath):
    if check_file_exists(filepath):
        os.remove(filepath)

# Makes a get request to specified url, and raises exception is something went wrong.
def make_request(url, *args):
    response = requests.get(url)

    response.raise_for_status()

    return response

# Retrieves the current game version from the supplied player data.
def parse_game_version(player_data):
    version = None

    try:
        version = player_data['last-played-version']['game_version']
    except Exception as e:
        print(e)

    return version

# Retrieves username from supplied player data.
def parse_username(player_data):
    username = None

    try:
        username = player_data['service-username']
    except Exception as e:
        print(e)

    return username

# Retrieves token from supplied player data.
def parse_token(player_data):
    token = None

    try:
        token = player_data['service-token']
    except Exception as e:
        print(e)

    return token

# List all mods files in the mods folder.
def list_mods_folder(mods_dir):
    return [mod_file for mod_file in list_dir(mods_dir) if '.zip' in mod_file]

# Retrieve mod name from mods filenames:
def parse_name(string):
    name_regex = re.compile('(\w+[- ]?)*_')
    mod_name = name_regex.search(string)
    if mod_name:
        return mod_name.group(0)[:-1]

# Retrieve version numbers from mods filenames.
def parse_version(string):
    version_regex = re.compile('_[0-9.]+\d+')
    version_number = version_regex.search(string)
    if version_number:
        return version_number.group(0)[1:]

# Retrieve mod names from mod-list.json.
def parse_mod_list(mod_list):
    return [mod['name'] for mod in mod_list if mod['enabled'] is True and mod['name'] != 'base']

# Merge mod names and version numbers and return objects in list.
def mods_version(mod_list, mod_files):
    mods = []

    active_mods = []
    for mod_name in mod_list:
        for mod_file in mod_files:
            if mod_name in mod_file:
                active_mods.append(mod_file)

    for mod in active_mods:
        name = parse_name(mod)
        version = parse_version(mod)
        if version and name:
            # mods.append((name, version))
            mods.append(
                dict(
                       name = name,
                    version = version
                )
            )

    return mods

# Requests mod information from url.
def fetch_mod_info(url, mod_name):
    mod_info = None

    try:
        mod_info = make_request(url + '/' + mod_name).json()
    except Exception as e:
        print(e)

    return mod_info

# Parses mod information and returns latest version.
def parse_mod_version(mod_info):
    latest_version = None

    try:
        for release in mod_info['releases']:
            if latest_version < release['version']:
                latest_version = release['version']
    except Exception as e:
        print(e)

    return latest_version

# Parses mod information and returns download link for latest version.
def parse_mod_download_info(mod_info, version):
    download_url, sha = None, None

    try:
        for release in mod_info['releases']:
            if version == release['version']:
                download_url = release['download_url']
                sha = release['sha1']
    except Exception as e:
        print(e)

    return download_url, sha

# Checks provided mod list for any updates.
def check_mods_for_updates(url, mods):
    updates_available = []

    for mod in mods:
        mod_name = mod['name']
        current_version = mod['version']

        print_update_check(mod_name)

        mod_info = fetch_mod_info(url, mod_name)

        latest_version = parse_mod_version(mod_info)
        download_url, sha = parse_mod_download_info(mod_info, latest_version)

        if current_version < latest_version:
            updates_available.append(
                dict(
                               name = mod_name,
                    current_version = current_version,
                        new_version = latest_version,
                       download_url = download_url,
                               sha1 = sha
                )
            )

    return updates_available

# Formats download url for mods.factorio.com.
def format_download_url(base_url, download_url, username, token):
    return '{base_url}{download_url}?username={username}&token={token}'.format(
                    base_url = base_url,
                    download_url = download_url,
                    username = username,
                    token = token)

# Checksums content with sha.
def confirm_sha_checksum(content, sha):
    m = hashlib.sha1()

    checksum = hashlib.sha1(content).hexdigest()

    return (checksum == sha)

# Formats filename using official factorio convention.
def format_mod_filename(name, version):
    return '{}_{}.zip'.format(name, version)

# Downloads updates, checksums, and writes to disk.
def download_updates(base_url, update_info, username, token, target_dir):
    downloaded = []

    for mod in update_info:
        url = format_download_url(base_url, mod['download_url'], username, token)
        update = make_request(url)

        if not confirm_sha_checksum(update.content, mod['sha1']):
            print('Checksum invalid for {}. Skipping update.'.format(mod['name']))
        else:
            print('Confirmed checksum for {}'.format(mod['name']))

            filename = format_mod_filename(mod['name'], mod['new_version'])
            filepath = os.path.join(target_dir, filename)

            write_file(filepath, update.content, 'wb')

            downloaded.append(filename)

            old_filename = format_mod_filename(mod['name'], mod['current_version'])
            old_filepath = os.path.join(target_dir, old_filename)
            remove_file(old_filepath)

    return downloaded

# Prints update checking proccess to the command line.
def print_update_check(mod_name):
    print('Checking mod {}..'.format(mod_name))

# Prints out any available updates.
def print_updates_available(updates):
    for update in updates:
        print('Update available for {}'.format(update['name']))


def main():
    PLAYER_SETTINGS = FACTORIO_DIR + PLAYER_DATA
    # PLAYER_SETTINGS = os.path.join(FACTORIO_DIR, PLAYER_DATA)
    MODS_DIR = FACTORIO_DIR + MOD_DIR
    # MODS_DIR = os.path.join(FACTORIO_DIR, MOD_DIR)
    MODS_LIST = MODS_DIR + MOD_LIST
    # MODS_LIST = os.path.join(MODS_DIR, MOD_LIST)

    MOD_INFO_URL = BASE_URL + API_ENDPOINT
    # UPDATE_SETTINGS = FACTORIO_DIR + MOD_DIR + SETTINGS_FILE


    if not check_dir_exists(FACTORIO_DIR):
        print('Could not find Factorio folder. Exiting..')
        return 1


    if not check_file_exists(PLAYER_SETTINGS):
        print('Could not find player-data.json. Exiting..')
        return 1

    player_data = open_file(PLAYER_SETTINGS, 'json')
    if player_data is None:
        print('Could not open player-data.json. Exiting..')
        return 1

    game_version = parse_game_version(player_data)
    if game_version is None:
        print('Could not find current game version. Exiting..')
        return 1

    username = parse_username(player_data)
    if username is None:
        print('Could not find username. Exiting..')
        return 1

    token = parse_token(player_data)
    if token is None:
        print('Could not find token. Exiting..')
        return 1


    print('Looking for mods..')

    if not check_dir_exists(MODS_DIR):
        print('Could not find any mods folder. Exiting..')
        return 1

    if not check_file_exists(MODS_LIST):
        print('Could not find mod-list.json. Exiting..')
        return 1

    mod_files = list_mods_folder(MODS_DIR)
    if mod_files == []:
        print('Could not find any files in mods folder. Exiting..')
        return 1

    mod_list = open_file(MODS_LIST, 'json')
    if mod_list is None:
        print('Could not find any mods listed in mod-list.json. Exiting..')
        return 1
    mod_list = mod_list["mods"]

    mods = parse_mod_list(mod_list)
    if mods == []:
        print('Could not parse mod list from mod-list.json. Exiting..')
        return 1

    mods_versions = mods_version(mods, mod_files)
    if mods_versions == []:
        print('Could not parse mods versions. Exiting..')
        return 1

    print('Found mod list!')


    updates_available = check_mods_for_updates(MOD_INFO_URL, mods_versions)
    if updates_available == []:
        print('No mod updates available at the moment.')
        return 1

    if updates_available:
        print('Found updates!')
        print_updates_available(updates_available)

    if download_updates(BASE_URL, updates_available, username, token, MODS_DIR) == []:
        print('Update unsuccessful. Did not apply any updates.')
        return 1


    print('Update complete.')
    return 0


if __name__ == '__main__':
    sys.exit(main())
